---
layout: markdown_page
title: "Category Direction - User Management"
description: Lifecycle management of users, from provisioning to de-provisioning and everything in between
canonical_path: "/direction/manage/auth/user-management/"
---

- TOC
{:toc}

## User Management

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Manage](/direction/dev/#manage) | [Viable](/direction/maturity/) | `2022-03-11` |

### Overview



#### Target Audience





#### What’s Next & Why




#### What is Not Planned Right Now



#### Maturity


### User Success Metrics

### Competitive landscape


### Top Vision issue(s)


## How you can help



